$(document).ready(function() {
		
	//Menu
	$('#abrir-menu').click(function() {
		if ($('#nav').hasClass("ativo")) {
			$('#nav').removeClass("ativo");
			$('#mask').removeClass("ativo");
			$(this).removeClass("ativo");
		} else {
			$('#nav').addClass("ativo");
			$('#mask').addClass("ativo");
			$(this).addClass("ativo");
		}
	});
	
	//Treeview http://mbraak.github.io/jqTree/
	var data = [{
		name: 'Petição',
		children: [
			{ name: 'Petição inicial' },
			{ name: 'Defesa do Policial Militar' }
		]
	},
	{
		name: 'Procuração',
		children: [
			{ name: 'Advogado Flavio Diniz' }
		]
	},
	{
		name: 'Documentos diversos',
		children: [
			{ name: 'Comprovante de residência' },
			{ name: 'Laudo de exame' },
			{ name: 'Pericia Médica' }
		]
	},
	{
		name: 'Certidão',
		children: [
			{ name: 'Certidão de casamento' },
			{ name: 'Certidão de antecedentes criminais' },
			{ name: 'Certidão de nada consta' }
		]
	},
	{
		name: 'Carta',
		children: [
			{ name: 'Carta precatória' },
			{ name: 'Carta do advogado' }
		]
	},
	{
		name: 'Procuração',
		children: [
			{ name: '111' }
		]
	},
	{
		name: 'Documentos diversos',
		children: [
			{ name: '111' }
		]
	},
	{
		name: 'Certidão',
		children: [
			{ name: '111' }
		]
	},
	{
		name: 'Carta',
		children: [
			{ name: '111' }
		]
	}];
    $('#treeview').tree({
        data: data
    });
	
	//Modal
	$('#myModal').on('shown.bs.modal', function () {
		$('#myInput').trigger('focus');
	})

});

function duplicar(id) {
	var f = $("#duplicar-" + id), c = f.clone(true,true);
	c.insertAfter(f).prepend("<hr/>");
}

